/**
 * @file    Scrambler.cpp
 * @authors <FILL ME IN ACCORDINGLY>
 * @brief   Scrambler implementation.
 */


#include <sstream>

#include "Scrambler.h"

Scrambler::Scrambler(const char a[], size_t beginIndex, size_t endIndex)
    : begin(beginIndex), end(endIndex) {
    // TODO: Copy the given array into the data member array
}

int Scrambler::length() const {
    // TODO: Compute me correctly.
    return 0;
}

std::string Scrambler::scramble() {
    // TODO: Call reverse() correctly. That is, this should just
    // return reverse(..., ..., ...);
    // You are tasked with determined what the arguments need to be for correct
    // operation.
    return "";
}

std::string Scrambler::toString() const {
    // TODO: Create a std::string representation of the character data stored
    // by this Scrambler.
    return "";
}


Scrambler::~Scrambler() {
    // No op... leave me alone.
}

// Private data member implementations
std::string Scrambler::reverse(const char anArray[], int first, int last) {
    // TODO: Implement me as per specification.
    return "";
}
