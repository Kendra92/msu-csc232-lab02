/**
 * @file   main.cpp
 * @author jdaehn
 * @brief  Entry point to this application.
 */

#include <cstdlib>
#include "Scrambler.h"

using namespace std;

/**
 * Entry point to this application.
 *
 * @param argc the number of command line arguments
 * @param argv an array of the command line arguments
 * @return EXIT_SUCCESS upon successful completion
 */
int main(int argc, char** argv) {
    const char initData[] = {'a', 'b', 'c'};
    Scrambler scrambler(initData, 0, 3);
    cout << "Scrambler is holding a character array of length: "
            << scrambler.length() << endl;
    cout << scrambler.toString() << endl;
    cout << scrambler.scramble() << endl;
    cout << scrambler.toString() << endl;

    return EXIT_SUCCESS;
}
