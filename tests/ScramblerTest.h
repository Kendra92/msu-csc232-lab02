/**
 * @file   ScramblerTest.h
 * @author Jim Daehn
 * @brief  Specification of Scrambler Unit Test. DO NOT MODIFY THE CONTENTS 
 * OF THIS FILE! ANY MODIFICATION TO THIS FILE WILL RESULT IN A GRADE OF 0 FOR
 * THIS LAB!
 */

#ifndef SCRAMBLERTEST_H
#define SCRAMBLERTEST_H

#include <cppunit/extensions/HelperMacros.h>
#include "../Scrambler.h"

static const char initData[] = {'a', 'b', 'c'};

class ScramblerTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(ScramblerTest);

    CPPUNIT_TEST(testLength);
    CPPUNIT_TEST(testScramble);
    CPPUNIT_TEST(testToString);

    CPPUNIT_TEST_SUITE_END();

public:
    ScramblerTest();
    virtual ~ScramblerTest();
    void setUp();
    void tearDown();

private:
    void testLength();
    void testScramble();
    void testToString();
    Scrambler *scrambler;
};

#endif /* SCRAMBLERTEST_H */
